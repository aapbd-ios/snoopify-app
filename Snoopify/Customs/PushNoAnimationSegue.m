//
//  PushNoAnimationSegue.m
//  Asia Happy Hour
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 MAC. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue
-(void) perform{
    [[[self sourceViewController] navigationController] pushViewController:[self   destinationViewController] animated:NO];
}
@end
