//
//  StickerVC.h
//  Snoopify
//
//  Created by MAC on 9/3/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StickerVCDelegate;
@interface StickerVC : UIViewController
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property(nonatomic,strong)IBOutlet UIView *containerView1;
@property(nonatomic,weak)id<StickerVCDelegate> delegate;
-(IBAction)crossButtonAction:(id)sender;
@property(nonatomic,strong)IBOutlet UIImageView *imageView1,*imageView2,*imageView3,*imageView4,*imageView5,*imageView6,*imageView7,*imageView8;
@end

@protocol StickerVCDelegate <NSObject>

@optional
-(void)selectImage:(UIImage*)image;
@end