//
//  EditorVC.h
//  Snoopify
//
//  Created by MAC on 8/27/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewClass.h"
#import "StickerVC.h"
#import "ViewControl.h"

@interface EditorVC : UIViewController<StickerVCDelegate>{
    ViewClass *controlView;
     ViewControl *viewControl;
    int imageTag;
}
@property(nonatomic,assign)BOOL editorModeOn;
@property(nonatomic,strong)NSMutableArray *imageViewArray;
@property(nonatomic,strong)UIImage *originalImage;
@property(nonatomic,strong)IBOutlet UIImageView *ediorImageView;
@property(nonatomic,strong)IBOutlet UIView *containerView;

@property(nonatomic,strong)IBOutlet UIView *controlEditPanelView;
@property(nonatomic,strong)IBOutlet UIButton *stickerAddButton;

-(IBAction)addSticker:(id)sender;
-(IBAction)doneButtonAction:(id)sender;
-(IBAction)clearButtonAction:(id)sender;
-(IBAction)newButtonAction:(id)sender;

@end
