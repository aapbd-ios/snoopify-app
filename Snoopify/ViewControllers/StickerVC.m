//
//  StickerVC.m
//  Snoopify
//
//  Created by MAC on 9/3/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "StickerVC.h"

@implementation StickerVC
-(IBAction)crossButtonAction:(id)sender;{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap1:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap2:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap3:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap4:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap5:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap6:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap7:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)handleTap8:(UITapGestureRecognizer *)sender
{
    UIImageView *imageView=(UIImageView*)sender.view;
    if ([self.delegate respondsToSelector:@selector(selectImage:)]) {
        [self.delegate selectImage:imageView.image];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.containerView.layer.cornerRadius=5.0;
    self.containerView.layer.borderColor=[UIColor clearColor].CGColor;
    self.containerView.layer.borderWidth=2.0;
    
    self.containerView1.layer.cornerRadius=5.0;
    self.containerView1.layer.borderColor=[UIColor clearColor].CGColor;
    self.containerView1.layer.borderWidth=2.0;
    
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap1:)];
    [self.imageView1 addGestureRecognizer:tapRecognizer1];
    
    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2:)];
    [self.imageView2 addGestureRecognizer:tapRecognizer2];
    
    UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap3:)];
    [self.imageView3 addGestureRecognizer:tapRecognizer3];
    
    UITapGestureRecognizer *tapRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap4:)];
    [self.imageView4 addGestureRecognizer:tapRecognizer4];
    
    UITapGestureRecognizer *tapRecognizer5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap5:)];
    [self.imageView5 addGestureRecognizer:tapRecognizer5];
    
    UITapGestureRecognizer *tapRecognizer6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap6:)];
    [self.imageView6 addGestureRecognizer:tapRecognizer6];
    
    UITapGestureRecognizer *tapRecognizer7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap7:)];
    [self.imageView7 addGestureRecognizer:tapRecognizer7];
    
    UITapGestureRecognizer *tapRecognizer8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap8:)];
    [self.imageView8 addGestureRecognizer:tapRecognizer8];
    [self prefersStatusBarHidden];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
