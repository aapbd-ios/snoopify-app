//
//  ViewController.m
//  Snoopify
//
//  Created by MAC on 8/27/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "HomeVC.h"

@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)shareButtonAction:(id)sender{
    
    NSString *shareText = @"Share text goes here...";
    NSArray *itemsToShare = @[shareText];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        UIViewController *vc = self.view.window.rootViewController;
        [vc presentViewController:activityVC animated:YES completion:nil];
    }
    //if iPad
    else
    {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        //NSLog(@"%f",self.view.frame.size.width/2);
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}
-(IBAction)nRButtonAction:(id)sender{
    
}
-(IBAction)snopifyLogoButtonAction:(id)sender{
    
}
-(IBAction)twitterFollowButtonAction:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/SnoopifyFHS?lang=en"]];
}
-(IBAction)instagramFollowButtonAction:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://instagram.com/SnoopifyFHS?lang=en"]];
}
-(IBAction)selectPhoto:(id)sender{
    [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
-(IBAction)takePhoto:(id)sender{
    [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
}

-(void)openWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.allowsEditing = NO;
    pickerController.sourceType = sourceType;
    pickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if (IS_IOS_8_OR_LATER) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:pickerController animated:YES completion:nil];
        }];
    }
    else
        [self presentViewController:pickerController animated:YES completion:nil];
}

//=================================================================
#pragma mark - ImagePicker Delegates
//=================================================================
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
       UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:chosenImage cropFrame:CGRectMake(5, 100.0f, self.view.frame.size.width, self.view.frame.size.width+50) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self.navigationController pushViewController:imgCropperVC animated:NO];/*
        [self.navigationController presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
            [self performSegueWithIdentifier:@"editorSegue" sender:self];
        }];*/
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    //self.bgImageView.image = editedImage;
    self.originalImage=editedImage;
    [self performSegueWithIdentifier:@"editorSegue" sender:self];
    /*
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
     */
}
- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"editorSegue"]) {
        EditorVC *vc = (EditorVC*)segue.destinationViewController;
        vc.originalImage=self.originalImage;
    }
}
@end
